#!/bin/sh

# Pull docker image
docker pull registry.gitlab.com/jiahaojz/birthday-api/hello:main

# Create database if it is not present already
echo "Creating database..."
if test -f ./database/birthday.db; then 
    echo "Database created, skipping creation process.."; 
else
    python3 ./flaskr/init-db.py;
fi

# Run docker container with volume mount
echo "Starting docker container..."
docker run -d -v $(pwd)/database:/app/database -p 5000:5000 registry.gitlab.com/jiahaojz/birthday-api/hello:main